# [RDM](https://github.com/RedisInsight/RedisDesktopManager) - Win+Mac

## RDM 简介
- Redis Desktop Manager，简称rdm，是一款可以跨平台的redis可视化工具，该工具可以说很大程度上弥补了memcached这类key/value存储的不足，为Java、C/C++、C#、PHP、JavaScript、Perl、Object-C、Python、Ruby、Erlang等开发语言提供了便利的客户端。一款好用的Redis桌面管理工具，支持命令控制台操作，以及常用，查询key，rename，delete等操作。
- 0.9.4 版本之后开始收费，想要使用免费版本需要自己对原码进行编译。官方编译教程。
- 自 2020.5.137 版本开始，Redis Desktop Manager 已更名为 RDM。
- 自 2022.0 版本开始，RDM 已更名为 RESP.app。您可以在#5170中找到更多详细信息。
- 这里提供免费的对源码编译后的 Windows 和 Mac 版本下载。

## [RedisInsight](https://redis.com/redis-enterprise/redis-insight/)
- 在 2022 年，Redis 与 RESP.app 的创建者 Igor Malinovskiy 联手创建了 RedisInsight。
- [RedisInsight 亮点](https://github.com/RedisInsight/RedisInsight#redisinsight-highlights)
    - 支持 SSH 隧道
    - 支持 [Redis Stack](https://redis.io/docs/about/about-stack/)
    - 数据库分析和性能改进建议
    - 高级 CLI 与语法高亮显示和自动完成
    - ......
- 有兴趣的可以尝试下载安装，目前暂没有中文界面！

## 备用链接
https://suyin-tools.lanzoui.com/b05y20rwb 密码：2dle



